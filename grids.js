var grids = {
    rotterdam : {
        points : [
            {note: 64, lat: 51.92837, lon: 4.41509},
            {note: 67, lat: 51.92837, lon: 4.44777},
            {note: 70, lat: 51.92837, lon: 4.48046},
            {note: 61, lat: 51.92837, lon: 4.51314},

            {note: 71, lat: 51.91086, lon: 4.43145},
            {note: 62, lat: 51.91086, lon: 4.46413},
            {note: 65, lat: 51.91086, lon: 4.49682},
            {note: 68, lat: 51.91086, lon: 4.52950},

            {note: 66, lat: 51.89335, lon: 4.44780},
            {note: 69, lat: 51.89335, lon: 4.48048},
            {note: 60, lat: 51.89335, lon: 4.51317},
            {note: 63, lat: 51.89335, lon: 4.54585}
        ],
        size: 2250
    }
};

module.exports.grids = grids;
