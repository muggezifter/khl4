\version "2.18.2"

#(set-default-paper-size "a4" 'landscape)

\header {
  title = "KHL 4"
  tagline = "Gert Rietveld, Rotterdam, 2015"
}

gridPath =
#'((moveto 109 -20)
 (lineto 10 -20)
 (lineto 26.5 -48.5)
 (lineto 43 -20)
 (lineto 59.5 -48.5)
 (lineto 76 -20)
 (lineto 92.5 -48.5)
 (lineto 109 -20)
 (lineto 125.5 -48.5)
 (lineto 26.5 -48.5)
 (lineto 43  -77)
 (lineto 59.5 -48.5)
 (lineto 76  -77)
 (lineto 92.5 -48.5)
 (lineto 109  -77)
 (lineto 125.5 -48.5)
 (lineto 142  -77)
 (lineto 43  -77)
 (lineto 10 -20)
 (closepath))



\markup {
  \combine % 0
        \translate #'(1 . 1) % 0.1.1.1
        \override #'(filled . #t) 
        \with-color #grey
        \path #0.15 #gridPath     
    \combine % 0.2
      \combine % 0.2.1
        \combine % 0.2.1.1
          \combine % 0.2.1.1.1
            % chord 1: 
            %\translate #'(21.5 . -28)
            \translate #'(21.5 . -25)
             \with-color #grey
            \score {
              \new Staff \with { \remove "Time_signature_engraver" }
              {
                \override Score.BarLine #'stencil = ##f
                <g' e' b'>1
              }
              \layout { ragged-right = ##t  indent = 0\cm }
            }	
            % chord 2: 
            %\translate #'(38 . -38 )
            \translate #'(38 . -33 )
            \with-color #white
            \score {
              \new Staff \with { \remove "Time_signature_engraver" }
              {
                \override Score.BarLine #'stencil = ##f
                <d' g' b'>1
              }
              \layout { ragged-right = ##t  indent = 0\cm }
            } 
          \combine % 0.2.1.1.2
            % chord 3: 
            \translate #'(54.5 . -25)
             \with-color #grey
            \score {
              \new Staff \with { \remove "Time_signature_engraver" }
              {
                \override Score.BarLine #'stencil = ##f
                <d' g' bes'>1
              }
              \layout { ragged-right = ##t  indent =0\cm }
            }
            % chord 4: 
            \translate #'(71 . -33)
            \with-color #white
            \score {
              \new Staff \with { \remove "Time_signature_engraver" }
              {
                \override Score.BarLine #'stencil = ##f
                <d' f' bes'>1
              }
              \layout { ragged-right = ##t  indent =0\cm }
            }
        \combine % 0.2.1.2
          \combine % 0.2.1.2.1
            % chord 5: 
            \translate #'(87.5 . -25)
            \with-color #grey
            \score {
              \new Staff \with { \remove "Time_signature_engraver" }
              {

                \override Score.BarLine #'stencil = ##f
                <des' f' bes'>1
              }
              \layout { ragged-right = ##t  indent = 0\cm }
            }

            % chord 6: 
            \translate #'(104 . -33)
            \with-color #white
            \score {
              \new Staff \with { \remove "Time_signature_engraver" }
              {

                \override Score.BarLine #'stencil = ##f
                <des' f' as'>1
              }
              \layout { ragged-right = ##t  indent = 0\cm }
            }
          \combine % 0.2.1.2.2
            % chord 7: 
            \translate #'(38 . -53.5)
            \with-color #grey
            \score {
              \new Staff \with { \remove "Time_signature_engraver" }
              {
                \override Score.BarLine #'stencil = ##f
                <b' d' fis'>1
              }
              \layout { ragged-right = ##t  indent = 0\cm }
            }
            % chord 8:  
            \translate #'(54.5 . -61.5)
            \with-color #white
            \score {

              \new Staff \with { \remove "Time_signature_engraver" }
              {
                \override Score.BarLine #'stencil = ##f
                <a' d' fis'>1
              }
              \layout { ragged-right = ##t  indent = 0\cm }
            }
      \combine % 0.2.2
        \combine % 0.2.2.1
          % chord 9: 
          \translate #'(71 . -53.5)
           \with-color #grey
          \score {
            \new Staff \with { \remove "Time_signature_engraver" }
            {
              \override Score.BarLine #'stencil = ##f
              <a' d' f'>1
            }
            \layout { ragged-right = ##t  indent = 0\cm }
          }
          % chord 10: 
          \translate #'(87.5 . -61.5)
          \with-color #white
          \score {
            \new Staff \with { \remove "Time_signature_engraver" }
            {
              \override Score.BarLine #'stencil = ##f
              <a' c' f'>1
            }
            \layout { ragged-right = ##t  indent = 0\cm }
          }
        \combine % 0.2.2.2
          % chord 11: 
          \translate #'(104 . -53.5)
          \with-color #grey
          \score {
            \new Staff \with { \remove "Time_signature_engraver" }
            {
              \override Score.BarLine #'stencil = ##f
              <as' c' f'>
            }
            \layout { ragged-right = ##t  indent = 0\cm }
          }
          % chord 12: 
           \translate #'(120.5 . -61.5)
           \with-color #white
           \score {
            \new Staff \with { \remove "Time_signature_engraver" }
            {
              \override Score.BarLine #'stencil = ##f
              <as' c' es'>
            }
            \layout { ragged-right = ##t  indent = 0\cm }
          }
}






