\version "2.16.2"


    {
      \override Staff.TimeSignature #'stencil = ##f
      \override Score.BarLine #'stencil = ##f
      < c' e' g' >1
    }


\markup {
  \translate #'(20 . 3)
  \circle {
    66
  }
}

% http://lilypond.org/doc/v2.18/Documentation/notation/graphic

%      http://www.lilypond.org/doc/v2.19/Documentation/snippets/staff-notation
%     Inserting score fragments above a staff as markups
    