// Kesterenstraat 51.89335, 4.44780

// 51.91059, 4.46498
var GeographicLib = require("geographiclib");

var geod = GeographicLib.Geodesic.WGS84, r;

var distance = 2250; // meters


function LatLon(lat, lon){
	this.lat = lat;
	this.lon = lon;
}

var points = [];


points[8] = new LatLon('51.89335000', '4.44780000');

var start = points[8];

r =  geod.Direct(start.lat, start.lon, 100, distance);
points[9] = new LatLon(r.lat2.toFixed(8),r.lon2.toFixed(8));

r =  geod.Direct(start.lat, start.lon, 100, distance * 2);
points[10] = new LatLon(r.lat2.toFixed(8),r.lon2.toFixed(8));

r =  geod.Direct(start.lat, start.lon, 100, distance * 3);
points[11] = new LatLon(r.lat2.toFixed(8),r.lon2.toFixed(8));

// up 1
r =  geod.Direct(start.lat, start.lon, -30, distance * 2);
points[0] = new LatLon(r.lat2.toFixed(8),r.lon2.toFixed(8));

r =  geod.Direct(start.lat, start.lon, -30, distance);
points[4] = new LatLon(r.lat2.toFixed(8),r.lon2.toFixed(8));

start = points[9];

r =  geod.Direct(start.lat, start.lon, -30, distance * 2);
points[1] = new LatLon(r.lat2.toFixed(8),r.lon2.toFixed(8));

r =  geod.Direct(start.lat, start.lon, -30, distance);
points[5] = new LatLon(r.lat2.toFixed(8),r.lon2.toFixed(8));

start = points[10];

r =  geod.Direct(start.lat, start.lon, -30, distance * 2);
points[2] = new LatLon(r.lat2.toFixed(8),r.lon2.toFixed(8));

r =  geod.Direct(start.lat, start.lon, -30, distance);
points[6] = new LatLon(r.lat2.toFixed(8),r.lon2.toFixed(8));

start = points[11];

r =  geod.Direct(start.lat, start.lon, -30, distance * 2);
points[3] = new LatLon(r.lat2.toFixed(8),r.lon2.toFixed(8));

r =  geod.Direct(start.lat, start.lon, -30, distance );
points[7] = new LatLon(r.lat2.toFixed(8),r.lon2.toFixed(8));



console.log(points);

// Find the point 20000 km SW of Perth, Australia (32.06S, 115.74E)...
r = geod.Direct(51.89335, 4.44780, 30, distance);

console.log("The position is (" +
            r.lat2.toFixed(8) + ", " + r.lon2.toFixed(8) + ").");
// This prints "The position is (32.11195529, -63.95925278)."