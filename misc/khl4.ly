\version "2.18.2"

#(set-default-paper-size "a4" 'landscape)

\header {
  title = "KHL 4"
  tagline = "Gert Rietveld, Rotterdam, 2015"
}

gridPath =
#'((moveto 109 -20)
 (lineto 10 -20)
 (lineto 26.5 -48.5)
 (lineto 43 -20)
 (lineto 59.5 -48.5)
 (lineto 76 -20)
 (lineto 92.5 -48.5)
 (lineto 109 -20)
 (lineto 125.5 -48.5)
 (lineto 26.5 -48.5)
 (lineto 43  -77)
 (lineto 59.5 -48.5)
 (lineto 76  -77)
 (lineto 92.5 -48.5)
 (lineto 109  -77)
 (lineto 125.5 -48.5)
 (lineto 142  -77)
 (lineto 43  -77)
 (lineto 10 -20)
 (closepath))



\markup {
  \combine % 0
    \combine % 0.1
      \combine % 0.1.1
        \translate #'(1 . 1) % 0.1.1.1
        %\override #'(filled . #t) 
        \path #0.15 #gridPath
        \combine % 0.1.1.2
          \combine % 0.1.1.2.1
            \combine % 0.1.1.2.1.1
              \translate #'(11 . -19)
               \with-color #white
               \draw-circle #1.8 #0.15 ##t
               \translate #'(9.8 . -19.8)
               \circle 64
             \combine % 0.1.1.2.1.2
              \translate #'(44 . -19)
               \with-color #white
               \draw-circle #1.8 #0.15 ##t
               \translate #'(42.8 . -19.8)
               \circle 67                  
          \combine % 0.1.1.2.2
            \combine % 0.1.1.2.2.1
              \translate #'(77 . -19)
               \with-color #white
               \draw-circle #1.8 #0.15 ##t
               \translate #'(75.8 . -19.8)
               \circle 70
             \combine % 0.1.1.2.2.2
              \translate #'(110 . -19)
               \with-color #white
               \draw-circle #1.8 #0.15 ##t
               \translate #'(108.8 . -19.8)
               \circle 61 
      \combine % 0.1.2    
        \combine % 0.1.2.1         
          \combine % 0.1.2.1.1
            \combine % 0.1.2.1.1.1
              \translate #'(27.5 . -47.5)
               \with-color #white
               \draw-circle #1.8 #0.15 ##t
               \translate #'(26.3 . -48.3)
               \circle 71
            \combine % 0.1.2.1.1.2
              \translate #'(60.5 . -47.5)
               \with-color #white
               \draw-circle #1.8 #0.15 ##t
               \translate #'(59.3 . -48.3)
               \circle 62
          \combine % 0.1.2.1.2
            \combine % 0.1.2.1.2.1
              \translate #'(93.5 . -47.5)
               \with-color #white
               \draw-circle #1.8 #0.15 ##t
               \translate #'(92.3 . -48.3)
               \circle 65
            \combine % 0.1.2.1.2.2
              \translate #'(126.5 . -47.5)
               \with-color #white
               \draw-circle #1.8 #0.15 ##t
               \translate #'(125.3 . -48.3)
               \circle 68
            %\translate #'(92.5 . -48.5)
            %\circle 65
            %\translate #'(125.5 . -48.5)
            %\circle 68
        \combine % 0.1.2.2     
          \combine % 0.1.2.2.1 
            \combine % 0.1.2.2.1.1
              \translate #'(44 . -76)
              \with-color #white
              \draw-circle #1.8 #0.15 ##t
              \translate #'(42.8 . -76.8)
              \circle 66
            \combine % 0.1.2.2.1.2
              \translate #'(77 . -76)
              \with-color #white
              \draw-circle #1.8 #0.15 ##t
              \translate #'(75.8 . -76.8)
              \circle 69
            %\translate #'(43 . -77)
            %\circle 66
            %\translate #'(76 . -77)
            %\circle 69
          \combine % 0.1.2.2.2 
            \combine % 0.1.2.2.2.1
              \translate #'(110 . -76)
              \with-color #white
              \draw-circle #1.8 #0.15 ##t
              \translate #'(108.8 . -76.8)
              \circle 60
            \combine % 0.1.2.2.2.2
              \translate #'(143 . -76)
              \with-color #white
              \draw-circle #1.8 #0.15 ##t
              \translate #'(141.8 . -76.8)
              \circle 63
            %\translate #'(109 . -77)
            %\circle 60
            %\translate #'(142 . -77)
            %\circle 63
    \combine % 0.2
      \combine % 0.2.1
        \combine % 0.2.1.1
          \combine % 0.2.1.1.1
            % chord 1: 
            %\translate #'(21.5 . -28)
            \translate #'(21.5 . -25)
            \score {
              \new Staff \with { \remove "Time_signature_engraver" }
              {
                \override Score.BarLine #'stencil = ##f
                <g' e' b'>1
              }
              \layout { ragged-right = ##t  indent = 0\cm }
            }	
            % chord 2: 
            %\translate #'(38 . -38 )
            \translate #'(38 . -33 )
            \score {
              \new Staff \with { \remove "Time_signature_engraver" }
              {
                \override Score.BarLine #'stencil = ##f
                <d' g' b'>1
              }
              \layout { ragged-right = ##t  indent = 0\cm }
            } 
          \combine % 0.2.1.1.2
            % chord 3: 
            \translate #'(54.5 . -25)
            \score {
              \new Staff \with { \remove "Time_signature_engraver" }
              {
                \override Score.BarLine #'stencil = ##f
                <d' g' bes'>1
              }
              \layout { ragged-right = ##t  indent =0\cm }
            }
            % chord 4: 
            \translate #'(71 . -33)
            \score {
              \new Staff \with { \remove "Time_signature_engraver" }
              {
                \override Score.BarLine #'stencil = ##f
                <d' f' bes'>1
              }
              \layout { ragged-right = ##t  indent =0\cm }
            }
        \combine % 0.2.1.2
          \combine % 0.2.1.2.1
            % chord 5: 
            \translate #'(87.5 . -25)
            \score {
              \new Staff \with { \remove "Time_signature_engraver" }
              {

                \override Score.BarLine #'stencil = ##f
                <des' f' bes'>1
              }
              \layout { ragged-right = ##t  indent = 0\cm }
            }

            % chord 6: 
            \translate #'(104 . -33)
            \score {
              \new Staff \with { \remove "Time_signature_engraver" }
              {

                \override Score.BarLine #'stencil = ##f
                <des' f' as'>1
              }
              \layout { ragged-right = ##t  indent = 0\cm }
            }
          \combine % 0.2.1.2.2
            % chord 7: 
            \translate #'(38 . -53.5)
            \score {
              \new Staff \with { \remove "Time_signature_engraver" }
              {
                \override Score.BarLine #'stencil = ##f
                <b' d' fis'>1
              }
              \layout { ragged-right = ##t  indent = 0\cm }
            }
            % chord 8:  
            \translate #'(54.5 . -61.5)
            \score {

              \new Staff \with { \remove "Time_signature_engraver" }
              {
                \override Score.BarLine #'stencil = ##f
                <a' d' fis'>1
              }
              \layout { ragged-right = ##t  indent = 0\cm }
            }
      \combine % 0.2.2
        \combine % 0.2.2.1
          % chord 9: 
          \translate #'(71 . -53.5)
          \score {
            \new Staff \with { \remove "Time_signature_engraver" }
            {
              \override Score.BarLine #'stencil = ##f
              <a' d' f'>1
            }
            \layout { ragged-right = ##t  indent = 0\cm }
          }
          % chord 10: 
          \translate #'(87.5 . -61.5)
          \score {
            \new Staff \with { \remove "Time_signature_engraver" }
            {
              \override Score.BarLine #'stencil = ##f
              <a' c' f'>1
            }
            \layout { ragged-right = ##t  indent = 0\cm }
          }
        \combine % 0.2.2.2
          % chord 11: 
          \translate #'(104 . -53.5)
          \score {
            \new Staff \with { \remove "Time_signature_engraver" }
            {
              \override Score.BarLine #'stencil = ##f
              <as' c' f'>
            }
            \layout { ragged-right = ##t  indent = 0\cm }
          }
          % chord 12: 
           \translate #'(120.5 . -61.5)
           \score {
            \new Staff \with { \remove "Time_signature_engraver" }
            {
              \override Score.BarLine #'stencil = ##f
              <as' c' es'>
            }
            \layout { ragged-right = ##t  indent = 0\cm }
          }
}






