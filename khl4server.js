var http = require('http');
var url = require('url');
var dispatcher = require('httpdispatcher');
var chord = require("./chord");
var recorder = require("./recorder");
var pd = require("./pd");


const PORT = 8080;

/**
 * Handle requests
 *
 * @param req
 * @param res
 */
var handleRequest = function (req, res) {
    try {
        //log the request on console
        console.log(Date.now() + ':' + req.url);
        //Dispatch
        dispatcher.dispatch(req, res);
    } catch (err) {
        console.log(err);
    }
}

/**
 * Get array of query parameters from the request
 */
var getQuery = function (req) {
    var url_parts = url.parse(req.url, true);
    return url_parts.query;
}


/**
 * Write output as jsonp (for app)
 */
var writeJsonp = function (res, callback, data) {
    res.writeHead(200, {'Content-Type': 'application/javascript'});
    res.end(callback + "(" + JSON.stringify(data) + ")");
}

/**
 * Write output as json (for debug)
 */
var writeJson = function (res, data) {
    res.writeHead(200, {'Content-Type': 'application/json'});
    res.end(JSON.stringify(data));
}

//For all static (js/css/images/etc.) set the directory name (relative path).
dispatcher.setStatic('front');
dispatcher.setStaticDirname('.');


/**
 * Routing: test server connection
 */
dispatcher.onGet("/test/server", function (req, res) {
    var query = getQuery(req);
    writeJsonp(res, query["callback"], { status: "OK"});
});

/**
 * Routing: test database connection
 */
dispatcher.onGet("/test/db", function (req, res) {
    var query = getQuery(req);
    recorder.count(function (err, result) {
        if (err) {
            console.log(err);
        }
        writeJsonp(res, query["callback"], { records: result});
    });
});


/**
 * Routing: play notes
 */
dispatcher.onGet("/play", function (req, res) {
    var query = getQuery(req);
    pd.play([].concat(query['n']),[].concat(query['v']));
    writeJsonp(res, query["callback"], { play: "OK"});
});

/**
 * Routing: start recording
 */
dispatcher.onGet("/recording/start", function (req, res) {
    var query = getQuery(req);
    recorder.start(function (err, result) {
        if (err) {
            console.log(err);
        }
        writeJsonp(res, query["callback"], result)
    });
});

/**
 * Routing: stop recording
 */
dispatcher.onGet("/recording/stop", function (req, res) {
    var query = getQuery(req);
    recorder.stop(query['rec_id'], function (err, result) {
        if (err) {
            console.log(err);
        }
        writeJsonp(res, query["callback"], result);
    });
});


/**
 * Routing: retrieve a list of recordings
 */
dispatcher.onGet("/recording/list", function (req, res) {
    var query = getQuery(req);
    recorder.list(function (err, result) {
        if (err) {
            console.log(err);
        }
        writeJsonp(res, query["callback"], result)
    });
});

/**
 * Routing: retrieve a recording (for playback)
 */
dispatcher.onGet("/recording/find", function (req, res) {
    var query = getQuery(req);
    recorder.find(query['rec_id'], function (err, result) {
        if (err) {
            console.log(err);
        }
        writeJsonp(res, query["callback"], result)
    });
});

/**
 * Routing: if recording, write a new node. Play the resulting chord
 */
dispatcher.onGet("/recording/node", function (req, res) {
    var query = getQuery(req);

    if (query['debug']) {
        writeJson(res, chord.compute(query['lat'], query['lon'], Date.now(), true))
    } else {
        var ch = chord.compute(query['lat'], query['lon'], Date.now(), false);
        recorder.record(query['rec_id'], ch, query['lat'], query['lon']);
        writeJsonp(res, query["callback"], ch)
    }

});

/**
 * Show the grid on a google map
 */
dispatcher.onGet("/map", function (req, res) {
    require('fs').readFile("./misc/geo/index.html", function (err, file) {
        if (err) {
            dispatcher.errorListener(req, res);
            return;
        }
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(file, 'binary');
        res.end();
    });
});


/**
 * Show the front end
 */
dispatcher.onGet("/", function (req, res) {
    require('fs').readFile("./front/index.html", function (err, file) {
        if (err) {
            dispatcher.errorListener(req, res);
            return;
        }
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(file, 'binary');
        res.end();
    });
});


/**
 * Create a server
 */
var server = http.createServer(handleRequest);

/**
 * Start the server
 */
server.listen(PORT, function () {
    //Callback triggered when server is successfully listening.!
    console.log("Server listening on: http://localhost:%s", PORT);
});









