module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt); // npm install --save-dev load-grunt-tasks

    grunt.initConfig({
        watch: {
            scripts: {
                files: '**/*.jsx',
                tasks: ['babel'],
                options: {
                    interrupt: true,
                }
            }
        },
        babel: {
            options: {
                sourceMap: true,
                presets: ['es2015','react']
            },
            dist: {
                files: {
                    'front/front.js': 'src/front.jsx'
                }
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['watch']);

};